
import { RouterModule, Routes } from '@angular/router';
import {
    AboutComponent,
    HomepageComponent,
    InsurancesListComponent,
    AccountComponent,
    LoginComponent,
    SignUpComponent
} from "./components/index.pages";


const app_routes: Routes = [
    {path: 'homepage', component: HomepageComponent},
    {path: 'about', component: AboutComponent },
    {path: 'insuranceList', component: InsurancesListComponent },
    {path: 'account', component: AccountComponent },
    {path: 'login', component: LoginComponent },
    {path: 'signup', component: SignUpComponent },
    {path: '**', pathMatch: 'full', redirectTo:'homepage'}
];

export const app_routing = RouterModule.forRoot(app_routes, { useHash: true });