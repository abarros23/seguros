
export { AboutComponent } from "./about/about.component";
export { HomepageComponent } from "./homepage/homepage.component";
export { InsurancesListComponent } from "./insurancesList/insurancesList.component";
export { AccountComponent } from "./account/account.component";
export { LoginComponent } from "./login/login.component";
export { SignUpComponent } from "./sign-up/sign-up.component";